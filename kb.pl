:- dynamic maxId/1.

maxId(14).

:- dynamic rule/3.

rule(1, [A|_], 'Kanena provlima anoksias') :-
	number(A),
	A>=20.
rule(2, [A, nai|_], 'Sovaro provlima anoksias') :-
	number(A),
	A<20.
rule(3, [A, ohi|_], 'Polu sovaro provlima anoksias') :-
	number(A),
	A<20.
rule(4, [_, _, arketa|_], 'Metria anoksia').
rule(5, [_, _, liga|_], 'Polu sovaro provlima anoksias').
rule(6, [_, _, ohi, maura_skoura, nai|_], 'Polu sovaro provlima anoksias').
rule(7, [_, _, ohi, maura_skoura, ohi, nai|_], 'Polu sovaro provlima anoksias').
rule(8, [_, _, ohi, maura_skoura, ohi, ohi|_], 'Sovaro provlima anoksias').
rule(9, [_, _, ohi, anoihta, _, _, mesaio_megalo|_], 'Kanena provlima anoksias').
rule(10, [_, _, ohi, anoihta, _, _, kanoniko, ohi], 'Kanena provlima anoksias').
rule(11, [_, _, ohi, anoihta, _, _, kanoniko, nai], 'Metria anoksia').
rule(13, [c, c, c, c, c, c, c, c], c).
rule(14, [ohi, n, n, n, n, n, n, n], nnnnnnnnnnnnnnnnnnn).
