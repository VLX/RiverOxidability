:- encoding(utf8).
:- set_prolog_flag(encoding, utf8).
:- set_prolog_flag(double_quotes, chars).
/* =-=-=-=-=-=-=-=-=-= ENCODING =-=-=-=-=-=-=-=-=-= */
/* ===============================================================================================================
                                                     HANDSHAKE
   =============================================================================================================== */
/* =-=-=-=-=-=-=-=-=-= WARNINGS =-=-=-=-=-=-=-=-=-= */
:- style_check(-singleton).

/* -=-=-=-=-=-=-=-=-= LIBRARIES =-=-=-=-=-=-=-=-=-= */
:- use_module(library(http/thread_httpd)).
:- use_module(library(http/http_dispatch)).
:- use_module(library(http/html_write)).
:- use_module(library(http/http_error)).
:- use_module(library(http/http_parameters)).
:- use_module(library(http/http_server_files)).
:- use_module(library(http/http_header)).
:- use_module(library(http/html_head)).
:- use_module(library(http/http_files)).
:- use_module(library(lists)).

/* -=-=-=-=-=-=-=-=-= GUI HANDLERS =-=-=-=-=-=-=-=-=-= */
:- http_handler('/page/css/business-casual.css',  http_reply_file('page/css/business-casual.css', []), []).
:- http_handler('/page/img/bg.jpg',  http_reply_file('page/img/bg.jpg', []), []).
:- http_handler('/page/img/slide-1.jpg',  http_reply_file('page/img/slide-1.jpg', []), []).
:- http_handler('/page/img/slide-2.jpg',  http_reply_file('page/img/slide-2.jpg', []), []).
:- http_handler('/page/img/slide-3.jpg',  http_reply_file('page/img/slide-3.jpg', []), []).
:- http_handler('/page/img/kb.jpg',  http_reply_file('page/img/kb.jpg', []), []).
:- http_handler('/page/vendor/bootstrap/css/bootstrap.min.css',  http_reply_file('page/vendor/bootstrap/css/bootstrap.min.css', []), []).
:- http_handler('/page/vendor/bootstrap/js/bootstrap.bundle.min.js',  http_reply_file('page/vendor/bootstrap/js/bootstrap.bundle.min.js', []), []).
:- http_handler('/page/vendor/jquery/jquery.min.js',  http_reply_file('page/vendor/jquery/jquery.min.js', []), []).

/* -=-=-=-=-=-=-=-= PREDICATE HANDLERS =-=-=-=-=-=-=-= */
:- http_handler(root('.'),landing_page('Hello'), []).
:- http_handler(root('diagn'), diagn, []).
:- http_handler(root('diagn_sol'), diagn_sol, []).
:- http_handler(root('exit'), exit, []).
:- http_handler(root('updatekb'), updatekb, []).
:- http_handler(root('del'), del, []).
:- http_handler(root('del_rec'), del_rec, []).
:- http_handler(root('add'), add, []).
:- http_handler(root('add_rec'), add_rec, []).
:- http_handler(root('edit'), edit, []).
:- http_handler(root('edit_rec'), edit_rec, []).
:- http_handler(root('save_exit'), save_exit, []).

/* -=-=-=-=-=-=-=-=-=-= SERVER PORT =-=-=-=-=-=-=-=-=-= */
tutorial_port(8080).

/* ===============================================================================================================
                                                        PAYLOAD
   =============================================================================================================== */

/*
   ********************** START POINT **********************
!important: SWI PROLOG TERMINAL COMMAND: "start.".
SWI PROLOG SERVER (DEFAULT) PORT: 8080.
*/
start :-
  %fortosi vasis gnosis
   ['kb.pl'],
   tutorial_port(PORT),
   http_server(http_dispatch, [port(PORT)]).

/*
  ****************** FIRST PAGE ******************
  Periehei to arhiko menu kai ena Image Carousel
*/
landing_page(_Request, Message):-
    % epilogi typou keimenou
  format('Content-type: text/html~n~n'),
  % ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">River Oxidability</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Problem Diagnosis Application and Knowledge Base Management</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">River Oxidability</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/diagn">Problem Diagnosis</a>
                          </li>
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/updatekb">Update Knowledge Base</a>
                          </li>
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/exit">Exit</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  <!-- Image Carousel -->
                  <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                      <ol class="carousel-indicators">
                          <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
                          <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
                      </ol>
                      <div class="carousel-inner" role="listbox">
                          <div class="carousel-item active">
                              <img class="d-block img-fluid w-100" src="page/img/slide-1.jpg" alt="">
                          </div>
                          <div class="carousel-item">
                              <img class="d-block img-fluid w-100" src="page/img/slide-2.jpg" alt="">
                          </div>
                          <div class="carousel-item">
                              <img class="d-block img-fluid w-100" src="page/img/slide-3.jpg" alt="">
                          </div>
                      </div>
                      <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                          <span class="sr-only">Previous</span>
                      </a>
                      <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                          <span class="carousel-control-next-icon" aria-hidden="true"></span>
                          <span class="sr-only">Next</span>
                      </a>
                  </div>
                  <!-- Welcome Message -->
              </div>
              <!-- /.container -->
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>
  </html>
']).

/*
  ****************** DIAGNOSIS PAGE ******************
  Eisagogi ton stoiheion gia tin diagnosi, ola ta pedia
  ektos to proto prepei na sublirothoun
*/
diagn(_Request):-
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  <form action="/diagn_sol" method="POST">
                      <div class="form-group">
                          <label for="usr">% Koresmos se dialimeno oksugono (arithmo/ohi)?:</label>
                          <input type="text" id="dial_oks" class="form-control" name="dial_oks">
                      </div>
                      <div class="form-group" id="no3" style="display: none">
                          <label for="usr">Yparksi NO3 (nai/ohi)?:</label>
                          <input type="text" id="no3in" class="form-control" name="no3">
                      </div>
                      <div class="form-group" id="oligo" style="display: none">
                          <label for="usr">Yparksi kokkinon oligochaetes (arketa/liga/ohi)?:</label>
                          <input type="text" id="oligoin" class="form-control" name="oligo">
                      </div>
                      <div class="form-group" id="izhma" style="display: none">
                          <label for="usr">Yparksi izhmaton (maura_skoura/anoihta)?:</label>
                          <input type="text" id="izhmain" class="form-control" name="izhma">
                      </div>
                      <div class="form-group" id="h2s" style="display: none">
                          <label for="usr">Yparksi H2S (nai/ohi)?:</label>
                          <input type="text" id="h2sin" class="form-control" name="h2s">
                      </div>
                      <div class="form-group" id="ch4" style="display: none">
                          <label for="usr">Yparksi CH4 (nai/ohi)?:</label>
                          <input type="text" id="ch4in" class="form-control" name="ch4">
                      </div>
                      <div class="form-group" id="ypostr" style="display: none">
                          <label for="usr">Typos ypostromatos (mesaio_megalo/kanoniko)?:</label>
                          <input type="text" id="ypostrin" class="form-control" name="ypostr">
                      </div>
                      <div class="form-group" id="murodia" style="display: none">
                          <label for="usr">To nero myrizei ashima (nai/ohi)?:</label>
                          <input type="text" id="murodiain" class="form-control" name="murodia">
                      </div>
                      <input type="submit" style="display: none" id="findp" class="form-control" value="Find Problem">

                  </form>
                  <button onclick="start()" class="form-control" id="probbutton">Next</Button>
              </div>
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script>
              function start()
              {

                  if (document.getElementById("dial_oks").value >= 20) {
                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }

                  if (document.getElementById("dial_oks").value < 20)
                      document.getElementById("no3").style.display = "block";
                  else
                      document.getElementById("no3").style.display = "none";

                  if (document.getElementById("no3in").value == "nai" ||
                          document.getElementById("no3in").value == "ohi") {

                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }

                  if (document.getElementById("dial_oks").value == "ohi")
                      document.getElementById("oligo").style.display = "block";
                  else
                      document.getElementById("oligo").style.display = "none";

                  if (document.getElementById("oligoin").value == "arketa" ||
                          document.getElementById("oligoin").value == "liga") {

                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }

                  if (document.getElementById("oligoin").value == "ohi")
                      document.getElementById("izhma").style.display = "block";
                  else
                      document.getElementById("izhma").style.display = "none";

                  if (document.getElementById("izhmain").value == "maura_skoura")
                      document.getElementById("h2s").style.display = "block";
                  else
                      document.getElementById("h2s").style.display = "none";

                  if (document.getElementById("h2sin").value == "nai") {
                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }

                  if (document.getElementById("h2sin").value == "ohi")
                      document.getElementById("ch4").style.display = "block";
                  else
                      document.getElementById("ch4").style.display = "none";

                  if (document.getElementById("ch4in").value == "nai" ||
                          document.getElementById("ch4in").value == "ohi") {
                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }

                  if (document.getElementById("izhmain").value == "anoihta")
                      document.getElementById("ypostr").style.display = "block";
                  else
                      document.getElementById("ypostr").style.display = "none";

                  if (document.getElementById("ypostrin").value == "mesaio_megalo") {
                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }

                  if (document.getElementById("ypostrin").value == "kanoniko")
                      document.getElementById("murodia").style.display = "block";
                  else
                      document.getElementById("murodia").style.display = "none";

                  if (document.getElementById("murodiain").value == "nai" ||
                          document.getElementById("murodiain").value == "ohi") {
                      document.getElementById("findp").style.display = "block";
                      document.getElementById("probbutton").style.display = "none";
                  }
              }
          </script>
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>
  </html>
']).

/*
  ****************** DIAGNOSIS RESULT PAGE ******************
  Se periptosi lathos stoiheion tote san apotelesma epistrefei
  diagnosi error me id 0
*/
diagn_sol(Request):-
  % sulegei tin pliroforia apo tin forma tis euresis anoksias
  http_parameters(Request, [
    dial_oks(Dial_oks, [default('NULL')]),
    no3(NO3, [default('NULL')]),
    oligo(Oligo, [default('NULL')]),
    izhma(Izhma, [default('NULL')]),
    h2s(H2S, [default('NULL')]),
    ch4(CH4, [default('NULL')]),
    ypostr(Ypostr, [default('NULL')]),
    murodia(Murodia, [default('NULL')])
    ]),
    % metatrepei to string tou arithmou se arithmo h se periptosi kenou paernei to ohi
  (atom_number(Dial_oks,Dial_oks_n);Dial_oks_n = ohi),
    % euresi tou provlimatos kai antlisi id kai suberasmatos provlimatos
   ((anoksia([Dial_oks_n,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia],[[Id,Result]]));Id=0,Result='Error'),
   % epilogi typou keimenou
 format('Content-type: text/html~n~n'),
 % ektyposh html periehomenou
   print_html(['
    <!DOCTYPE html>
    <html lang="en">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
            <meta name="description" content="">
            <meta name="author" content="">
            <title>Knowledge Systems</title>
            <!-- Bootstrap core CSS -->
            <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
            <!-- Custom fonts for this template -->
            <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
            <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
            <!-- Custom styles for this template -->
            <link href="page/css/business-casual.css" rel="stylesheet">
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        </head>
        <body>
            <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
            <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Results</div>
            <!-- Navigation -->
            <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
                <div class="container">
                    <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarResponsive">
                        <ul class="navbar-nav mx-auto">
                            <li class="nav-item px-lg-4">
                                <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="container">
                <div class="bg-faded p-4 my-4">
                    ID: ',Id,'<br>
                    Diagnosis: ',Result,'
                </div>
            </div>
            <footer class="bg-faded text-center py-5">
                <div class="container">
                    <p class="m-0">Vlachakis Vasileios 3998</p>
                </div>
            </footer>
            <!-- Bootstrap core JavaScript -->
            <script src="page/vendor/jquery/jquery.min.js"></script>
            <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        </body>
    </html>
']).

/*
  ****************** UPDATE PAGE ******************
  Periehei to deutero menu pou ehei shesi me tin
  enimorosi tis vasis gnosis kai mia eikona
*/
updatekb(_Request):-
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">River Oxidability</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Select Action</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">River Oxidability</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                          </li>
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/del">Delete Record</a>
                          </li>
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/add">Add Record</a>
                          </li>
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/edit">Edit Record</a>
                          </li>
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href="/save_exit">Save and Exit</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  <img class="d-block img-fluid w-100" src="page/img/kb.jpg" alt="">
              </div>
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>
  </html>
']).

/*
  ****************** DELETE PAGE ******************
  Eisagogi tou id gia tin diagrafi rule me vasi to id tou
*/
del(_Request):-
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  <form action="/del_rec" method="POST">
                      <div class="form-group">
                          <label for="usr">Record ID?:</label>
                          <input type="number" class="form-control" name="id" required>
                      </div>
                      <input type="submit" class="form-control" value="Delete Record">
                  </form>
              </div>
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>
  </html>
']).

/*
  ****************** DELETE REPORT PAGE ******************
  Se periptosi lathos tote san apotelesma epistrefei
  delete error
*/
del_rec(Request):-
  % lipsh tou id apo tin forma
  http_parameters(Request, [
    id(Rid, [default('NULL')])
  ]),
  % diagrafh tou rule me vash to id kai epistrofi apotelesmatos
  delete_record_KB(Rid,Check),
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  ',Check,'
              </div>
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>
  </html>
  ']).

/*
  ****************** ADD PAGE ******************
  Eisagogi olon ton stoiheion gia tin eisagogi rule
*/
add(_Request):-
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% pairnei to megisto id kanonon
  maxId(Max),
% tous auksanei kata 1 oste na to proteinei ston hristi na valei to amesos epomeno id
  Num is Max+1,
  % ektyposi html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  <form action="/add_rec" method="POST">
                      <div class="form-group">
                          <label for="usr">Eisagogi kanona olografos me morfi: rule(Rid,[Dial_oks,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia], Apantisi).</label><br>
                          <label for="usr">Paradeigma1:<br> rule(11, [_, _, ohi, anoihta, _, _, kanoniko, nai], &apos;Metria anoksia&apos;).</label><br>
                          <label for="usr">Paradeigma2:<br> rule(3, [A, ohi|_], &apos;Polu sovaro provlima anoksias&apos;) :-
                          	number(A),
                          	A<20.
                            </label><br>
                              <label for="usr">Hint: To Rid pou prepei na eisahtei einai to ',Num,'</label>
                          <input type="text" class="form-control" name="kanonas">
                      </div>
                      <input type="submit" class="form-control" value="Add Record">
                  </form>
              </div>
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
          <script type="text/javascript">

function stopRKey(evt) {
  var evt = (evt) ? evt : ((event) ? event : null);
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
}

document.onkeypress = stopRKey;

</script>
      </body>
  </html>
']).

/*
  ****************** ADD REPORT PAGE ******************
  Se periptosi lathos tote san apotelesma epistrefei
  delete error
*/
add_rec(Request):-
  % lipsi ton stoiheion apo tin forma prosthesis kanona
  http_parameters(Request, [
    kanonas(Kanonas, [default('NULL')])
    ]),
  % enimerosi tin vash gnosis kaqi epistrofi munhmatos apotelesmatos
  add_record_KB(Kanonas,Check),
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Knowledge Systems</title>
        <!-- Bootstrap core CSS -->
        <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom fonts for this template -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template -->
        <link href="page/css/business-casual.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
        <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
            <div class="container">
                <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="bg-faded p-4 my-4">
                ',Check,'
            </div>
        </div>
        <footer class="bg-faded text-center py-5">
            <div class="container">
                <p class="m-0">Vlachakis Vasileios 3998</p>
            </div>
        </footer>
        <!-- Bootstrap core JavaScript -->
        <script src="page/vendor/jquery/jquery.min.js"></script>
        <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
']).

/*
  ****************** EDIT PAGE ******************
  Eisagogi olon ton stoiheion gia tin tropopoihsh rule
  me vasi to id tou
*/
edit(_Request):-
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
  <html lang="en">
      <head>
          <meta charset="utf-8">
          <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
          <meta name="description" content="">
          <meta name="author" content="">
          <title>Knowledge Systems</title>
          <!-- Bootstrap core CSS -->
          <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
          <!-- Custom fonts for this template -->
          <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
          <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
          <!-- Custom styles for this template -->
          <link href="page/css/business-casual.css" rel="stylesheet">
          <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
          <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
      </head>
      <body>
          <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
          <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
          <!-- Navigation -->
          <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
              <div class="container">
                  <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                      <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarResponsive">
                      <ul class="navbar-nav mx-auto">
                          <li class="nav-item px-lg-4">
                              <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                          </li>
                      </ul>
                  </div>
              </div>
          </nav>
          <div class="container">
              <div class="bg-faded p-4 my-4">
                  <form action="/edit_rec" method="POST">
                  <div class="form-group">
                      <label for="usr">ID?:</label>
                      <input type="number" class="form-control" name="id" required>
                  </div>
                      <div class="form-group">
                          <label for="usr">% Koresmos se dialimeno oksugono (arithmo/keno)?:</label>
                          <input type="number" class="form-control" name="dial_oks">
                      </div>
                      <div class="form-group">
                          <label for="usr">Yparksi NO3 (nai/ohi)?:</label>
                          <input type="text" class="form-control" name="no3" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">Yparksi kokkinon oligochaetes (arketa/liga/ohi)?:</label>
                          <input type="text" class="form-control" name="oligo" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">Yparksi izhmaton (maura_skoura/anoihta)?:</label>
                          <input type="text" class="form-control" name="izhma" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">Yparksi H2S (nai/ohi)?:</label>
                          <input type="text" class="form-control" name="h2s" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">Yparksi CH4 (nai/ohi)?:</label>
                          <input type="text" class="form-control" name="ch4" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">Typos ypostromatos (mesaio_megalo/kanoniko)?:</label>
                          <input type="text" class="form-control" name="ypostr" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">To nero myrizei ashima (nai/ohi)?:</label>
                          <input type="text" class="form-control" name="murodia" required>
                      </div>
                      <div class="form-group">
                          <label for="usr">Keimeno Diagnosis?:</label>
                          <input type="text" class="form-control" name="apantisi" required>
                      </div>
                      <input type="submit" class="form-control" value="Edit Record">
                  </form>
              </div>
          </div>
          <footer class="bg-faded text-center py-5">
              <div class="container">
                  <p class="m-0">Vlachakis Vasileios 3998</p>
              </div>
          </footer>
          <!-- Bootstrap core JavaScript -->
          <script src="page/vendor/jquery/jquery.min.js"></script>
          <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      </body>
  </html>
']).

/*
  ****************** ADD REPORT PAGE ******************
  Se periptosi lathos tote san apotelesma epistrefei
  edit error
*/
edit_rec(Request):-
  % adlish olon ton stoiheion apo tin forma trpopoihsh kanona
  http_parameters(Request, [
    id(Rid, [default('NULL')]),
    dial_oks(Dial_oks, [default('NULL')]),
    no3(NO3, [default('NULL')]),
    oligo(Oligo, [default('NULL')]),
    izhma(Izhma, [default('NULL')]),
    h2s(H2S, [default('NULL')]),
    ch4(CH4, [default('NULL')]),
    ypostr(Ypostr, [default('NULL')]),
    murodia(Murodia, [default('NULL')]),
    apantisi(Apantisi, [default('NULL')])
    ]),
    % tropopoihsh kanona kai antikatastash tou me ta stoiheia tis formas
  edit_record_KB(Rid,[Dial_oks,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia,Apantisi],Check),
  % epilogi typou keimenou
format('Content-type: text/html~n~n'),
% ektyposh html periehomenou
  print_html(['
  <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Knowledge Systems</title>
        <!-- Bootstrap core CSS -->
        <link href="page/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Custom fonts for this template -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Slab:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic" rel="stylesheet" type="text/css">
        <!-- Custom styles for this template -->
        <link href="page/css/business-casual.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="tagline-upper text-center text-heading text-shadow text-white mt-5 d-none d-lg-block">Problem Diagnosis</div>
        <div class="tagline-lower text-center text-expanded text-shadow text-uppercase text-white mb-5 d-none d-lg-block">Input Your Data</div>
        <!-- Navigation -->
        <nav class="navbar navbar-expand-lg navbar-light bg-faded py-lg-4">
            <div class="container">
                <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Problem Diagnosis</a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav mx-auto">
                        <li class="nav-item px-lg-4">
                            <a class="nav-link text-uppercase text-expanded" href=".">Home</a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container">
            <div class="bg-faded p-4 my-4">
                ',Check,'
            </div>
        </div>
        <footer class="bg-faded text-center py-5">
            <div class="container">
                <p class="m-0">Vlachakis Vasileios 3998</p>
            </div>
        </footer>
        <!-- Bootstrap core JavaScript -->
        <script src="page/vendor/jquery/jquery.min.js"></script>
        <script src="page/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
']).

/*Vriskei to provlima anoksias kai epistrefei to id kai to keimeno tou rule*/
anoksia([Dial_oks,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia],Results):-
  bagof([Rid,Apantisi], rule(Rid,[Dial_oks,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia], Apantisi), Results).

exit(_Request):- halt.

/*metatrepei to id pou ekane eisagogi o hristis se arithmo
kai epeita to kanei retract, telos epistrefei to katalilo munhma*/
delete_record_KB(Rid,Check):-
  % metatropi apo string se arithmo
  ((atom_number(Rid,Ridn),
  % diagrafh tou kanona me tin vohtheia tou clause
  clause(rule(Ridn,[_,_,_,_,_,_,_,_],_), Body),
  retract(rule(Ridn,[_,_,_,_,_,_,_,_],_):- Body),
  % eggrafi ton apotelesmaton kai paraogi katallilou munumatos
  Check = 'Record Deleted Successful'), apothik_allagon;Check = 'Delete Error').

/*To katigorima maxid periehei to megalutero se arithmo rule id oste
na auksithei epeita kata ena kai na einai sigouro oti to kainourio id pou paraksame
den yparhei gia na min yparhoun rule me koino id.
metatrepei to id pou ekane eisagogi o hristis se arithmo
kai epeita to kanei assert, kanei retract to palio maxid, kanei assert to kainourio kai
telos epistrefei to katalilo munhma*/
add_record_KB(Kanonas,Check):-
  % euresh tou megistou id kai auksish kata ena
    ((maxId(L),Rid is L+1,
    % diagrafh tou paliou megistou id
    retract(maxId(L)),
    % eggrafh tou kainouriou megistou id
    assert(maxId(Rid)),
    % anoigma gia egrafh kb.pl
    tell('kb.pl'),
    % eggrafh ton 2 katigorimaton
    listing(maxId/1),
    listing(rule/3),
    % eggrafh se morfh string tou kanona pou edose o hristis
    write(Kanonas),nl,
    % telos eggrafon gia to kb.pl
    told,
    % epanafortosh tis vashs gnosis
    consult('kb'),
    % paragogi tou katallilou munhmatos epistrofis
    Check = 'Record Added Successful');
    Check = 'Add Error').

/*metatrepei to id pou ekane eisagogi o hristis se arithmo
kai epeita me vasi to id pou edose o hristis kanei retract to palio rule,
prosthetei to kainourio kai telos epistrefei to katalilo munhma*/
edit_record_KB(Rid,[Dial_oks,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia,Apantisi],Check):-
  % metatroph string se arithmo
    ((atom_number(Rid,Ridn),
    % diagrafh paliou katigorimatos me tin vohtheia tou clause
    clause(rule(Ridn,[_,_,_,_,_,_,_,_],_), Body),
    retract(rule(Ridn,[_,_,_,_,_,_,_,_],_):- Body),
    % metatroph string se arithmo gia to pososto oksithtas h eisagogh timhs ohi
    (atom_number(Dial_oks,Dial_oks_n);Dial_oks_n = ohi),
    % prosthesh neou kanona
    assert(rule(Ridn,[Dial_oks_n,NO3,Oligo,Izhma,H2S,CH4,Ypostr,Murodia], Apantisi)),
    % paragogi katalilou minhmatos kai egrafh sto kb.pl
    Check = 'Record Edited Successful'); Check = 'Edit Error'),
    apothik_allagon.

/*gia tin epilogi save and exit, apothikeuei tin vasi gnosis kai kleinei ton server*/
save_exit(_Request):- apothik_allagon,halt.

/*grafei sto arheio kb.pl tin torini katastasi tis vasis gnosis*/
apothik_allagon:- tell('kb.pl'),
  listing(maxId/1),listing(rule/3),told.
